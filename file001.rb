class Flyer
  attr_reader :name, :email, :miles_flown
  attr_accessor :status

  def initialize(name, email, miles_flown, status=:bronze)
    @name = name
    @email = email
    @miles_flown = miles_flown
    @status = status
  end
end

flyers = []
flyers << Flyer.new("Larry", "larry@example.com", 4000, :platinum)
flyers << Flyer.new("Moe", "moe@example.com", 1000)
flyers << Flyer.new("Curly", "curly@example.com", 3000, :gold)
flyers << Flyer.new("Shemp", "shemp@example.com", 2000)

platinum_flyers, normal_flyers = flyers.partition { |f| f.status == :platinum }
nametags           = flyers.map { |f| "#{f.name} (#{f.status.upcase})"}
distances_flown    = flyers.map { |f| f.miles_flown * 1.6 }
total_miles_flown  = flyers.map { |f| f.miles_flown }.reduce(:+)
bronze_total_miles = flyers.select { |f| f.status == :bronze }.map { |f| f.miles_flown }.reduce(:+)
